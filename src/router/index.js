import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

/* Layout */
import Layout from '@/layout'

/**
 * Note: sub-menu only appear when route children.length >= 1
 * Detail see: https://panjiachen.github.io/vue-element-admin-site/guide/essentials/router-and-nav.html
 *
 * hidden: true                   if set true, item will not show in the sidebar(default is false)
 * alwaysShow: true               if set true, will always show the root menu
 *                                if not set alwaysShow, when item has more than one children route,
 *                                it will becomes nested mode, otherwise not show the root menu
 * redirect: noRedirect           if set noRedirect will no redirect in the breadcrumb
 * name:'router-name'             the name is used by <keep-alive> (must set!!!)
 * meta : {
    roles: ['admin','editor']    control the page roles (you can set multiple roles)
    title: 'title'               the name show in sidebar and breadcrumb (recommend set)
    icon: 'svg-name'/'el-icon-x' the icon show in the sidebar
    breadcrumb: false            if set false, the item will hidden in breadcrumb(default is true)
    activeMenu: '/example/list'  if set path, the sidebar will highlight the path you set
  }
 */

/**
 * constantRoutes
 * a base page that does not have permission requirements
 * all roles can be accessed
 */
export const constantRoutes = [
  {
    path: '/login',
    component: () => import('@/views/login/index'),
    hidden: true
  },
  {
    path: '/register',
    component: () => import('@/views/register/index'),
    hidden: true
  },
  {
    path: '/404',
    component: () => import('@/views/404'),
    hidden: true
  },

  {
    path: '/',
    component: Layout,
    redirect: '/dashboard',
    children: [{
      path: 'dashboard',
      name: 'Dashboard',
      component: () => import('@/views/dashboard/index'),
      meta: { title: '控制面板', icon: 'dashboard' }
    }]
  },

  // {
  //   path: '/example',
  //   component: Layout,
  //   redirect: '/example/table',
  //   name: 'Example',
  //   meta: { title: 'Example', icon: 'el-icon-s-help' },
  //   children: [
  //     {
  //       path: 'table',
  //       name: 'Table',
  //       component: () => import('@/views/table/index'),
  //       meta: { title: 'Table', icon: 'table' }
  //     },
  //     {
  //       path: 'tree',
  //       name: 'Tree',
  //       component: () => import('@/views/tree/index'),
  //       meta: { title: 'Tree', icon: 'tree' }
  //     }
  //   ]
  // },
  {
    path: '/user',
    component: Layout,
    redirect: '/user/usersearch',
    name: 'User',
    meta: { title: '用户管理', icon: 'user' },
    children: [
      {
        path: 'usersearch',
        name: 'UserSearch',
        component: () => import('@/views/user/index'),
        meta: { title: '用户查询', icon: 'search' }
      },
      {
        path: 'useradd',
        name: 'UserAdd',
        component: () => import('@/views/user/add'),
        meta: { title: '添加用户', icon: 'plus' }
      }
    ]
  },
  {
    path: '/dept',
    component: Layout,
    redirect: '/dept/search',
    name: 'Dept',
    meta: { title: '部门管理', icon: 'edit' },
    children: [
      {
        path: 'search',
        name: 'DeptSearch',
        component: () => import('@/views/dept/index'),
        meta: { title: '部门查询', icon: 'search' }
      },
      {
        path: 'add',
        name: 'DeptAdd',
        component: () => import('@/views/dept/add'),
        meta: { title: '添加部门', icon: 'plus' }
      }
    ]
  },
  {
    path: '/job',
    component: Layout,
    redirect: '/job/search',
    name: 'Job',
    meta: { title: '职位管理', icon: 'light' },
    children: [
      {
        path: 'search',
        name: 'JobSearch',
        component: () => import('@/views/job/index'),
        meta: { title: '职位查询', icon: 'search' }
      },
      {
        path: 'add',
        name: 'JobtAdd',
        component: () => import('@/views/job/add'),
        meta: { title: '添加职位', icon: 'plus' }
      }
    ]
  },
  {
    path: '/employee',
    component: Layout,
    redirect: '/employee/search',
    name: 'Employee',
    meta: { title: '员工管理', icon: 'users' },
    children: [
      {
        path: 'search',
        name: 'EmployeeSearch',
        component: () => import('@/views/employee/index'),
        meta: { title: '员工查询', icon: 'search' }
      },
      {
        path: 'add',
        name: 'EmployeetAdd',
        component: () => import('@/views/employee/add'),
        meta: { title: '添加员工', icon: 'plus' }
      }
    ]
  },
  {
    path: '/role',
    component: Layout,
    redirect: '/role/search',
    name: 'Role',
    meta: { title: '角色管理', icon: 'role' },
    children: [
      {
        path: 'search',
        name: 'RoleSearch',
        component: () => import('@/views/role/index'),
        meta: { title: '角色查询', icon: 'search' }
      },
      {
        path: 'add',
        name: 'RoleAdd',
        component: () => import('@/views/role/add'),
        meta: { title: '添加角色', icon: 'plus' }
      }
    ]
  },
  {
    path: '/notice',
    component: Layout,
    redirect: '/notice/search',
    name: 'Notice',
    meta: { title: '公告管理', icon: 'notice' },
    children: [
      {
        path: 'search',
        name: 'NoticeSearch',
        component: () => import('@/views/notice/index'),
        meta: { title: '公告查询', icon: 'search' }
      },
      {
        path: 'add',
        name: 'NoticeAdd',
        component: () => import('@/views/notice/add'),
        meta: { title: '添加公告', icon: 'plus' }
      }
    ]
  },
  {
    path: '/file',
    component: Layout,
    redirect: '/file/search',
    name: 'File',
    meta: { title: '文件管理', icon: 'file' },
    children: [
      {
        path: 'search',
        name: 'FileSearch',
        component: () => import('@/views/file/index'),
        meta: { title: '文件查询', icon: 'search' }
      },
      {
        path: 'add',
        name: 'FileAdd',
        component: () => import('@/views/file/add'),
        meta: { title: '添加文件', icon: 'plus' }
      }
    ]
  },


  // {
  //   path: '/form',
  //   component: Layout,
  //   children: [
  //     {
  //       path: 'index',
  //       name: 'Form',
  //       component: () => import('@/views/form/index'),
  //       meta: { title: 'Form', icon: 'form' }
  //     }
  //   ]
  // },
  //
  // {
  //   path: '/nested',
  //   component: Layout,
  //   redirect: '/nested/menu1',
  //   name: 'Nested',
  //   meta: {
  //     title: 'Nested',
  //     icon: 'nested'
  //   },
  //   children: [
  //     {
  //       path: 'menu1',
  //       component: () => import('@/views/nested/menu1/index'), // Parent router-view
  //       name: 'Menu1',
  //       meta: { title: 'Menu1' },
  //       children: [
  //         {
  //           path: 'menu1-1',
  //           component: () => import('@/views/nested/menu1/menu1-1'),
  //           name: 'Menu1-1',
  //           meta: { title: 'Menu1-1' }
  //         },
  //         {
  //           path: 'menu1-2',
  //           component: () => import('@/views/nested/menu1/menu1-2'),
  //           name: 'Menu1-2',
  //           meta: { title: 'Menu1-2' },
  //           children: [
  //             {
  //               path: 'menu1-2-1',
  //               component: () => import('@/views/nested/menu1/menu1-2/menu1-2-1'),
  //               name: 'Menu1-2-1',
  //               meta: { title: 'Menu1-2-1' }
  //             },
  //             {
  //               path: 'menu1-2-2',
  //               component: () => import('@/views/nested/menu1/menu1-2/menu1-2-2'),
  //               name: 'Menu1-2-2',
  //               meta: { title: 'Menu1-2-2' }
  //             }
  //           ]
  //         },
  //         {
  //           path: 'menu1-3',
  //           component: () => import('@/views/nested/menu1/menu1-3'),
  //           name: 'Menu1-3',
  //           meta: { title: 'Menu1-3' }
  //         }
  //       ]
  //     },
  //     {
  //       path: 'menu2',
  //       component: () => import('@/views/nested/menu2/index'),
  //       name: 'Menu2',
  //       meta: { title: 'menu2' }
  //     }
  //   ]
  // },
  //
  // {
  //   path: 'external-link',
  //   component: Layout,
  //   children: [
  //     {
  //       path: 'https://panjiachen.github.io/vue-element-admin-site/#/',
  //       meta: { title: 'External Link', icon: 'link' }
  //     }
  //   ]
  // },

  // 404 page must be placed at the end !!!
  { path: '*', redirect: '/404', hidden: true }
]

const createRouter = () => new Router({
  // mode: 'history', // require service support
  scrollBehavior: () => ({ y: 0 }),
  routes: constantRoutes
})

const router = createRouter()

// Detail see: https://github.com/vuejs/vue-router/issues/1234#issuecomment-357941465
export function resetRouter() {
  const newRouter = createRouter()
  router.matcher = newRouter.matcher // reset router
}

export default router
