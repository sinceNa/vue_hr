/**
 * Created by PanJiaChen on 16/11/18.
 */

/**
 * @param {string} path
 * @returns {Boolean}
 */
export function isExternal(path) {
  return /^(https?:|mailto:|tel:)/.test(path)
}

/**
 * @param {string} str
 * @returns {Boolean}
 */
export function validUsername(str) {
  const valid_map = ['admin', 'editor']
  return valid_map.indexOf(str.trim()) >= 0
}

/**
 * 判断验证码是不是数字或者字母
 * @param {string} str
 * @returns {Boolean}
 */
export function validCaptcha(str) {
  return /^[0-9a-zA-Z]+$/.test(str)
}

/**
 * 判断是不是数字
 * @param {string} str
 * @returns {Boolean}
 */
export function validPhone(str) {
  return /^[0-9]+$/.test(str)
}
