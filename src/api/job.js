/**
 *
 */
import request from '@/utils/request'
const host='http://47.106.237.169:8080/hr_system'
import qs from 'qs'

export function deleteById(data){
	return request({
		url:host+'/job/deleteById',
		method:'get',
		params:data
	})
}

export function deleteManyByIdArray(data){
	return request({
		url:host+'/job/deleteManyByIdArray',
		method:'get',
		params:data,
    paramsSerializer: params => {
      return qs.stringify(params, { indices: false })
    }
	})
}
export function editJob(data){
	return request({
		url:host+'/job/edit',
		method:'post',
		data
	})
}
export function findAllJob(){
	return request({
		url:host+'/job/findAll',
		method:'get',
	})
}
export function findAllJobByCondition(data){
	return request({
		url:host+'/job/findAllByCondition',
		method:'get',
		params:data
	})
}

export function findById(data){
	return request({
		url:host+'/job/findOne',
		method:'get',
		params:data
	})
}
export function saveJob(data){
	return request({
		url:host+'/job/save',
		method:'post',
		data
	})
}
