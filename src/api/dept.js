/**
 *
 */
import request from '@/utils/request'
const host='http://47.106.237.169:8080/hr_system'
import qs from 'qs'



export function deleteDeptById(data){
	return request({
		url:host+'/dept/deleteById',
		method:'get',
		params:data
	})
}

export function deleteManyByIdArray(data){
	return request({
		url:host+'/dept/deleteManyByIdArray',
		method:'get',
		params:data,
    paramsSerializer: params => {
      return qs.stringify(params, { indices: false })
    }
	})
}
export function editDept(data){
	return request({
		url:host+'/dept/edit',
		method:'post',
		data
	})
}
export function findAllDept(){
	return request({
		url:host+'/dept/findAll',
		method:'get',
	})
}
export function findAllDeptByCondition(data){
	return request({
		url:host+'/dept/findAllByCondition',
		method:'get',
		params:data
	})
}

export function findById(data){
	return request({
		url:host+'/dept/findOne',
		method:'get',
		params:data
	})
}
export function save(data){
	return request({
		url:host+'/dept/save',
		method:'post',
		data
	})
}
