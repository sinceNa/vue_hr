/**
 *
 */
import request from '@/utils/request'
const host='http://47.106.237.169:8080/hr_system'
import qs from 'qs'


export function deleteEmpById(data){
	return request({
		url:host+'/employee/deleteById',
		method:'get',
		params:data
	})
}

export function deleteManyByIdArray(data){
	return request({
		url:host+'/employee/deleteManyByIdArray',
		method:'get',
		params:data,
    paramsSerializer: params => {
      return qs.stringify(params, { indices: false })
    }
	})
}
export function editEmp(data){
	return request({
		url:host+'/employee/edit',
		method:'post',
		data
	})
}
export function findAllEmp(data){
	return request({
		url:host+'/employee/findAll',
		method:'get',
    params:data
	})
}
export function findById(data){
	return request({
		url:host+'/employee/findOne',
		method:'get',
		params:data
	})
}
export function saveEmp(data){
	return request({
		url:host+'/employee/save',
		method:'post',
		data
	})
}

export function downloadExcel(data) {
return request({
  url:host+'/employee/downloadExcel',
  method:'get',
  params:data
})
}
