import request from '@/utils/request'
import qs from 'qs'

const host='http://47.106.237.169:8080/hr_system'

export function findAllRole() {
  return request({
    url: host+'/role/findAll',
    method: 'get'
  })
}

export function deleteOneRole(data) {
  return request({
    url: host+'/role/deleteById',
    method: 'get',
    params: data
  })
}

export function addRole(data) {
  return request({
    url: host+'/role/save',
    method: 'post',
    data
  })
}

export function findAllRoleByCondition(data) {
  return request({
    url: host+'/role/findAllByCondition',
    method: 'get',
    params: data
  })
}

export function editRole(data) {
  return request({
    url: host+'/role/edit',
    method: 'post',
    data
  })
}

export function deleteRoleList(data) {
  console.log(data)
  return request({
    url: host+'/role/deleteManyByIdArray',
    method: 'get',
    params:{
      rid:data
    },
    paramsSerializer: params => {
      return qs.stringify(params, { indices: false })
    }
  })
}
