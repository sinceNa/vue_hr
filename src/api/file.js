import request from '@/utils/request'
import qs from 'qs'

const host='http://47.106.237.169:8080/hr_system'

export function deleteById(data) {
  return request({
    url: host+'/file/deleteById',
    method: 'get',
    params:data
  })
}

export function findAllFile(data) {
  return request({
    url: host+'/file/findAllByCondition',
    method: 'get',
    params:data
  })
}

export function deleteManyByNumberArray(data) {
  return request({
    url: host+'/file/deleteManyByNumberArray',
    method: 'get',
    params:data,
    paramsSerializer: params => {
      return qs.stringify(params, { indices: false })
    }
  })
}
