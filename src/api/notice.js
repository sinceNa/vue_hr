/**
 *
 */
import request from '@/utils/request'
const host='http://47.106.237.169:8080/hr_system'
import qs from 'qs'



export function deleteById(data){
	return request({
		url:host+'/notice/deleteById',
		method:'get',
		params:data
	})
}

export function deleteManyByIdArray(data){
	return request({
		url:host+'/notice/deleteManyByIdArray',
		method:'get',
		params:data,
    paramsSerializer: params => {
      return qs.stringify(params, { indices: false })
    }
	})
}
export function edit(data){
	return request({
		url:host+'/notice/edit',
		method:'post',
		data
	})
}
export function findAllByCodition(data){
	return request({
		url:host+'/notice/findAllByCondition',
		method:'get',
		params:data
	})
}
export function findById(data){
	return request({
		url:host+'/notice/findOne',
		method:'get',
		params:data
	})
}
export function save(data){
	return request({
		url:host+'/notice/save',
		method:'post',
		data
	})
}
