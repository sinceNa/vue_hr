import request from '@/utils/request'
import qs from 'qs'

const host='http://47.106.237.169:8080/hr_system'

export function register(data) {
  return request({
    url: host+'/register',
    method: 'post',
    data
  })
}


export function login(data) {
  return request({
    url: host+'/login',
    method: 'post',
    data:{
      "username":data.username,
      "password":data.password,
      "checkCode":data.captcha
    }
  })
}

export function getInfo(number) {
  return request({
    url: host+'/user/findByNumber',
    method: 'get',
    params: {
      number:number
    }
  })
}

export function changeUserInfo(data) {
  return request({
    url: host+'/user/edit',
    method: 'post',
    data
  })
}

export function logout() {
  return request({
    url: host+'/logOut',
    method: 'get'
  })
}

export function getCaptcha() {
  return request({
    url:host+'/getCheckCode',
    method: 'get'
  })
}

export function getUserList(data) {
  return request({
    url:host+'/user/findAllByCondition',
    method: 'get',
    params: {
      number:data.number,
      pageNum:data.pageNum,
      pageSize:data.pageSize,
      phone:data.phone,
      roleId:data.roleId,
      username:data.username
    }
  })
}

export function deleteUserList(data) {
  return request({
    url:host+'/user/deleteManyByNumberArray',
    method:'get',
    params:{
      numberArray:data
    },
    paramsSerializer: params => {
      return qs.stringify(params, { indices: false })
    }

  })
}

export function deleteUserOne(data) {
  return request({
    url:host+'/user/deleteByNumber',
    method:'get',
    params:{
      number :data
    }

  })
}

export function editUserInfo(data) {
  return request({
    url:host+'/user/edit',
    method:'post',
    data

  })
}

export function addUser(data) {
  return request({
    url:host+'/user/save',
    method:'post',
    data
  })
}
export function welcome() {
  return request({
    url:host+'/welcome',
    method:'get'
  })
}
